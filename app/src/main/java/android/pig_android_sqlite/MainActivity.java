package android.pig_android_sqlite;

import android.app.AlertDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    DatabaseHelper myDb;
    EditText editNume, editPrenume, editGrupa, editTextId;
    Button btnAddData;
    Button btnviewAll;
    Button btnDelete;
    Button btnDelete_all;
    Button btnviewUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myDb = new DatabaseHelper(this);

        editNume = (EditText) findViewById(R.id.editText_nume);
        editPrenume = (EditText) findViewById(R.id.editText_prenume);
        editGrupa = (EditText) findViewById(R.id.editText_grupa);
        editTextId = (EditText) findViewById(R.id.editText_id);
        btnAddData = (Button) findViewById(R.id.button_add);
        btnviewAll = (Button) findViewById(R.id.button_viewAll);
        btnviewUpdate = (Button) findViewById(R.id.button_update);
        btnDelete = (Button) findViewById(R.id.button_delete);
        btnDelete_all = (Button) findViewById(R.id.button_delete_all);
        AddData();
        viewAll();
        UpdateData();
        DeleteData();
        DeleteData_ALL();
    }

    public void DeleteData_ALL(){
        btnDelete_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor res = myDb.getAllData();
                if (res.getCount() != 0) {
                    myDb.deleteData_all();
                    Toast.makeText(MainActivity.this, "Date sterse", Toast.LENGTH_LONG).show();
                }else {
                Toast.makeText(MainActivity.this, "Tabelul este gol", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void DeleteData() {
        btnDelete.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Integer deletedRows = myDb.deleteData(editTextId.getText().toString());
                        if (deletedRows > 0)
                            Toast.makeText(MainActivity.this, "Date adaugate", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(MainActivity.this, "Datele nu au fost adaugate", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public void UpdateData() {
        btnviewUpdate.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isUpdate = myDb.updateData(editTextId.getText().toString(),
                                editNume.getText().toString(),
                                editPrenume.getText().toString(), editGrupa.getText().toString());
                        if (isUpdate == true)
                            Toast.makeText(MainActivity.this, "Date adaugate", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(MainActivity.this, "Datele nu au fost adaugate", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public void AddData() {
        btnAddData.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isInserted = myDb.insertData(
                                editNume.getText().toString(),
                                editPrenume.getText().toString(),
                                editGrupa.getText().toString());
                        if (isInserted == true)
                            Toast.makeText(MainActivity.this, "Date adaugate", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(MainActivity.this, "Datele nu au fost adaugate", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public void viewAll() {
        btnviewAll.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Cursor res = myDb.getAllData();
                        if (res.getCount() == 0) {
                            showMessage("Error", "Nu exista date.");
                            return;
                        }

                        StringBuffer buffer = new StringBuffer();
                        while (res.moveToNext()) {
                            buffer.append("Id :" + res.getString(0) + "\n");
                            buffer.append("NUME :" + res.getString(1) + "\n");
                            buffer.append("PRENUME :" + res.getString(2) + "\n");
                            buffer.append("GRUPA :" + res.getString(3) + "\n\n");
                        }

                        showMessage("Data", buffer.toString());
                    }
                }
        );
    }

    public void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
}